package com.helpers;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LoggerHelper {
	private static boolean root = false;

	public static Logger getLogger(Class c) {
		if (root) {
			return Logger.getLogger(c);
		}

		PropertyConfigurator.configure("log4j.properties");

		root = true;

		return Logger.getLogger(c);
	}
}
