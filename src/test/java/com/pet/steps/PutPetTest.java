package com.pet.steps;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.helpers.LoggerHelper;
import com.pet.model.Category;
import com.pet.model.Pet;
import com.pet.model.Tag;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class PutPetTest {

	Logger log = LoggerHelper.getLogger(PutPetTest.class);

	Pet pet = new Pet();
	Response response;

	@Given("The PUT request for Pets is set")
	public void the_put_request_for_pets_is_set() {

		log.info("UPDATE Pet status to sold ----- STARTED");
		log.info("The PUT request for Pets is set");
		
		RestAssured.baseURI = "https://petstore.swagger.io";
		RestAssured.basePath = "/v2/pet";
	}

	@Given("To not override totally the existing Pet some Data must be provided again like the ID {int}")
	public void to_not_override_totally_the_existing_pet_some_data_must_be_provided_again_like_the_id(Integer petId) {

		log.info("Data of the Pet [ID]");
		
		pet.setId(petId);
	}

	@Given("Like its Category {int} {string}")
	public void like_its_category(Integer categoryId, String categoryName) {

		log.info("Data of the Pet [Category]");
		
		Category category = new Category(categoryId, categoryName);
		pet.setCategory(category);
	}

	@Given("Like its Name {string}")
	public void like_its_name(String petName) {
		
		log.info("Data of the Pet [Name]");
		
		pet.setName(petName);
	}

	@Given("Like its Photo Urls {string} {string}")
	public void like_its_photo_urls(String photoUrl1, String photoUrl2) {
		
		log.info("Data of the Pet [Photo URLs]");
		
		ArrayList<String> photoUrls = new ArrayList<String>();
		photoUrls.add(photoUrl1);
		photoUrls.add(photoUrl2);
		pet.setPhotoUrls(photoUrls);
	}

	@Given("Like its Tags {int} {string} {int} {string}")
	public void like_its_tags(Integer tag1Id, String tag1Name, Integer tag2Id, String tag2Name) {
		
		log.info("Data of the Pet [Tags]");
		
		ArrayList<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(tag1Id, tag1Name));
		tags.add(new Tag(tag2Id, tag2Name));
		pet.setTags(tags);
	}

	@Given("Like its Status {string}")
	public void like_its_status(String status) {
		
		log.info("New status 'Sold' for the Pet [Status]");
		
		pet.setStatus(status);
	}

	@When("The PUT API request is sent to update the existing Pet")
	public void the_put_api_request_is_sent_to_update_the_existing_pet() {

		log.info("The PUT request to update Pet status to sold");
		
		response = given().contentType(ContentType.JSON)
				.when().body(pet).put();
	}

	@Then("A correct response after updating status to {string} is received with status code {int}")
	public void a_correct_response_after_updating_status_to_sold_is_received_with_status_code(String petStatus, int code) {

		log.info("Checking the status code of the PUT request is " + code + "and Pet status is '"+petStatus+"'");
		
		if (code != response.statusCode()) {
			log.error("\t The status code of the PUT request is " + response.statusCode() + " and 200 was expected\n\n");
		}
		
		if (!(response.path("status").toString()).equalsIgnoreCase(petStatus)) {
			log.error("\t The Pet status is " + response.path("status").toString() + " and '"+petStatus+"' was expected\n\n");
		}
		
		assertEquals(code, response.statusCode());
		assertEquals(petStatus, response.path("status").toString());
		
		log.info("Response:\n"+response.prettyPrint().toString());

		log.info("UPDATE Pet status to sold ----- FINISHED\n\n\n");
	}
}
