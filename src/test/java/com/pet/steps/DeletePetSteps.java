package com.pet.steps;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;

import com.helpers.LoggerHelper;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class DeletePetSteps {
    
    Logger log = LoggerHelper.getLogger(DeletePetSteps.class);

	Response response;
    
    @Given("The DELETE request to remove the Pet is set")
    public void the_delete_request_to_remove_the_pet_is_set() {

    	log.info("DELETE the added Pet in the POST request ----- STARTED");
		log.info("The DELETE request to remove the Pet is set");
		
		RestAssured.baseURI = "https://petstore.swagger.io";
		RestAssured.basePath = "/v2/pet";
    }

    @When("The DELETE API request is sent with the Pet ID {int}")
    public void the_delete_api_request_is_sent_with_the_pet_id(Integer petId) {
		
		log.info("The DELETE request to remove the added Pet is sent");
		
    	response = given()
                .when()
                .delete("/"+petId);
    }

    @Then("A correct response after deleting the Pet By ID is received with status code {int}")
    public void a_correct_response_after_deleting_the_pet_by_id_is_received_with_status_code(int code) {
		
		log.info("Checking the status code of the DELETE request is " + code);
		
		if (code != response.statusCode()) {
			log.error("\t The status code of the DELETE request is " + response.statusCode() + " and 200 was expected\n\n");
		}
		assertEquals(code, response.statusCode());
		
		log.info("Response:\n"+response.prettyPrint().toString());
    }
    
    @When("The GET API request is sent with the Pet ID {int} to check its deletion")
    public void the_get_api_request_is_sent_with_the_pet_id_to_check_its_deletion(Integer petId) {
        
		log.info("The GET request to check the Pet is not found after its deletion.");
		
    	response = given()
                .when()
                .get("/"+petId);
    }

    @Then("A valid response after GET Pet By ID is received with status code {int}")
    public void a_valid_response_after_get_pet_by_id_is_received_with_status_code(int code) {
        
    	log.info("Checking the status code of the GET request is " + code);
		
		if (code != response.statusCode()) {
			log.error("\t The status code of the GET request is " + response.statusCode() + " and 400 was expected\n\n");
		}
		assertEquals(code, response.statusCode());
		
		log.info("Response:\n"+response.prettyPrint().toString());

		log.info("DELETE the added Pet in the POST request ----- FINISHED\n\n\n");
    }

}
