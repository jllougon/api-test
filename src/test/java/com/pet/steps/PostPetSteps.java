package com.pet.steps;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.helpers.LoggerHelper;
import com.pet.model.Category;
import com.pet.model.Pet;
import com.pet.model.Tag;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class PostPetSteps {
	
	Logger log = LoggerHelper.getLogger(PostPetSteps.class);
	
	Pet pet = new Pet();
	Response response;
	
	@Given("The POST request for Pets is set")
	public void the_post_request_for_pets_is_set() {

		log.info("CREATE new Pet ----- STARTED");
		log.info("The POST request for Pets is set");
		
		RestAssured.baseURI = "https://petstore.swagger.io";
		RestAssured.basePath = "/v2/pet";
	}
	
	@And("Data for the new Pet is provided like the ID {int}")
	public void data_for_the_new_pet_is_provided_like_the_id(Integer petId) {
		
		log.info("Data for the new Pet [ID]");
		
		pet.setId(petId);
	}

	@And("Like Category {int} {string}")
	public void like_category(Integer categoryId, String categoryName) {
		
		log.info("Data for the new Pet [Category]");
		
		Category category = new Category(categoryId, categoryName);
		pet.setCategory(category);
	}

	@And("Like Name {string}")
	public void like_name(String petName) {
		
		log.info("Data for the new Pet [Name]");
		
		pet.setName(petName);
	}

	@And("Like Photo Urls {string} {string}")
	public void like_photo_urls_www_google_com(String photoUrl1, String photoUrl2) {
		
		log.info("Data for the new Pet [Photo URLs]");
		
		ArrayList<String> photoUrls = new ArrayList<String>();
		photoUrls.add(photoUrl1);
		photoUrls.add(photoUrl2);
		pet.setPhotoUrls(photoUrls);
	}

	@And("Like Tags {int} {string} {int} {string}")
	public void like_tags(Integer tag1Id, String tag1Name, Integer tag2Id, String tag2Name) {
		
		log.info("Data for the new Pet [Tags]");
		
		ArrayList<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(tag1Id, tag1Name));
		tags.add(new Tag(tag2Id, tag2Name));
		pet.setTags(tags);
	}

	@And("Like Status {string}")
	public void like_status(String status) {
		
		log.info("Data for the new Pet [Status]");
		
		pet.setStatus(status);
	}

	@When("The POST API request is sent to add a new Pet")
	public void the_post_api_request_is_sent_to_add_a_new_pet() {
		
		log.info("The POST request to add the new Pet is sent");
		
		response = given().contentType(ContentType.JSON)
				.when().body(pet).post();
	}

	@Then("A correct response is received with status code {int}")
	public void a_correct_response_is_received_with_status_code(int code) {
		
		log.info("Checking the status code of the POST request is " + code);

		if (code != response.statusCode()) {
			log.error("\t The status code of the POST request is " + response.statusCode() + " and 200 was expected\n\n");
		}
		assertEquals(code, response.statusCode());
		
		log.info("Response:\n"+response.prettyPrint().toString());

		log.info("CREATE new Pet ----- FINISHED\n\n\n");
	}
}
