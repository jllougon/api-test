Feature: CRUD operations for Pets API

  Scenario: Create a new Pet
    Given The POST request for Pets is set
    And Data for the new Pet is provided like the ID <petId>
    And Like Category <categoryId> "<categoryName>"
    And Like Name "<petName>"
    And Like Photo Urls "<photoUrl1>" "<photoUrl2>"
    And Like Tags <tag1Id> "<tag1Name>" <tag2Id> "<tag2Name>"
    And Like Status "<petStatus>"
    When The POST API request is sent to add a new Pet
    Then A correct response is received with status code 200

    Examples: 
      | petId  		|	categoryId	|	categoryName	|	petName		|	photoUrl1				|	photoUrl2					|	tag1Id		|	tag1Name	|	tag2Id		|	tag2Name	|	petStatus	|
      | 12345678 	| 33333333		|	Category01		|	PetPost01	| www.google.com	|	www.wikipedia.org	| 44444444	|	Tag01			|	55555555	|	Tag02			|	available	|

      
	Scenario: Check the new Pet added
    Given The GET request to obtain the added Pet by its ID is set
    When The GET API request is sent with the Pet ID <petId>
    Then A correct response after GET Pet By ID is received with status code 200
    
    Examples: 
      | petId  		|
      | 12345678 	|
      
	Scenario: Update the added Pet status to sold
    Given The PUT request for Pets is set
    And To not override totally the existing Pet some Data must be provided again like the ID <petId>
    And Like its Category <categoryId> "<categoryName>"
    And Like its Name "<petName>"
    And Like its Photo Urls "<photoUrl1>" "<photoUrl2>"
    And Like its Tags <tag1Id> "<tag1Name>" <tag2Id> "<tag2Name>"
    And Like its Status "<petStatus>"
    When The PUT API request is sent to update the existing Pet
    Then A correct response after updating status to "<petStatus>" is received with status code 200

    Examples: 
      | petId  		|	categoryId	|	categoryName	|	petName		|	photoUrl1				|	photoUrl2					|	tag1Id		|	tag1Name	|	tag2Id		|	tag2Name	|	petStatus	|
      | 12345678 	| 33333333		|	Category01		|	PetPost01	| www.google.com	|	www.wikipedia.org	| 44444444	|	Tag01			|	55555555	|	Tag02			|	sold			|

	Scenario: Delete the added Pet
    Given The DELETE request to remove the Pet is set
    When The DELETE API request is sent with the Pet ID <petId>
    Then A correct response after deleting the Pet By ID is received with status code 200
    When The GET API request is sent with the Pet ID <petId> to check its deletion
    Then A valid response after GET Pet By ID is received with status code 404
    
    Examples: 
      | petId  		|
      | 12345678 	|
